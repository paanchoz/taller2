package testcalc;

public class Calculadora {

	public static int suma(double a, double b) {
		return (int) (a+b);
	}
	public static int resta(double a, double b) {
		return (int) (a-b);
	}
	public static int multiplicacion(double a, double b) {
		return (int) (a*b);
	}
	public static int division(double a, double b) {
		return (int) (a/b);
	}
}
