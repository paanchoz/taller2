package testcalc;

import static org.junit.Assert.*;

import org.junit.Test;

public class testing {
	
//***** TEST PARA SUMA ******
	@Test
	public void testSumaPositivos() {
		int resultado = Calculadora.suma(2,3);
		int esperado = 5;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testSumaNegativos() {
		int resultado = Calculadora.suma(-2,-3);
		int esperado = -5;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testSumaPositivoNegativo() {
		int resultado = Calculadora.suma(2,-3);
		int esperado = -1;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testSumaNegativoPositivo() {
		int resultado = Calculadora.suma(-2,3);
		int esperado = 1;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testSumaCeroPositivo() {
		int resultado = Calculadora.suma(0,3);
		int esperado = 3;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testSumaCeroNegativo() {
		int resultado = Calculadora.suma(0,-3);
		int esperado = -3;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testSumaPositivoCero() {
		int resultado = Calculadora.suma(2,0);
		int esperado = 2;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testSumaNegativoCero() {
		int resultado = Calculadora.suma(-2,0);
		int esperado = -2;
		assertEquals(esperado, resultado);
	}
	
//***** TEST PARA RESTA *****
	@Test
	public void testRestaPositivos() {
		int resultado = Calculadora.resta(2,3);
		int esperado = -1;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testRestaNegativos() {
		int resultado = Calculadora.resta(-2,-3);
		int esperado = 1;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testRestaPositivoNegativo() {
		int resultado = Calculadora.resta(2,-3);
		int esperado = 5;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testRestaNegativoPositivo() {
		int resultado = Calculadora.resta(-2,3);
		int esperado = -5;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testRestaCeroPositivo() {
		int resultado = Calculadora.resta(0,3);
		int esperado = -3;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testRestaCeroNegativo() {
		int resultado = Calculadora.resta(0,-3);
		int esperado = 3;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testRestaPositivoCero() {
		int resultado = Calculadora.resta(2,0);
		int esperado = 2;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testRestaNegativoCero() {
		int resultado = Calculadora.resta(-2,0);
		int esperado = -2;
		assertEquals(esperado, resultado);
	}
	
//***** TEST PARA MULTIPLICACIÓN *****
	@Test
	public void testMultiplicacionPositivos() {
		int resultado = Calculadora.multiplicacion(2,3);
		int esperado = 6;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testMultiplicacionNegativos() {
		int resultado = Calculadora.multiplicacion(-2,-3);
		int esperado = 6;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testMultiplicacionPositivoNegativo() {
		int resultado = Calculadora.multiplicacion(2,-3);
		int esperado = -6;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testMultiplicacionNegativoPositivo() {
		int resultado = Calculadora.multiplicacion(-2,3);
		int esperado = -6;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testMultiplicacionCeroPositivo() {
		int resultado = Calculadora.multiplicacion(0,3);
		int esperado = 0;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testMultiplicacionCeroNegativo() {
		int resultado = Calculadora.multiplicacion(0,-3);
		int esperado = 0;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testMultiplicacionPositivoCero() {
		int resultado = Calculadora.multiplicacion(2,0);
		int esperado = 0;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testMultiplicacionNegativoCero() {
		int resultado = Calculadora.multiplicacion(-2,0);
		int esperado = 0;
		assertEquals(esperado, resultado);
	}
	
//***** TEST PARA DIVISIÓN ******
	@Test
	public void testDivisionPositivos() {
		int resultado = Calculadora.division(5,5);
		int esperado = 1;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testDivisionNegativos() {
		int resultado = Calculadora.division(-5,-5);
		int esperado = 1;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testDivisionPositivoNegativo() {
		int resultado = Calculadora.division(5,-5);
		int esperado = -1;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testDivisionNegativoPositivo() {
		int resultado = Calculadora.division(-5,5);
		int esperado = -1;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testDivisionCeroPositivo() {
		int resultado = Calculadora.division(0,5);
		int esperado = 0;
		assertEquals(esperado, resultado);
	}
	@Test
	public void testDivisionCeroNegativo() {
		int resultado = Calculadora.division(0,-5);
		int esperado = 0;
		assertEquals(esperado, resultado);
	}
}
